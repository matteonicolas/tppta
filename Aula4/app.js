console.log("Hello World!");
// 

/*
*/
let nome = "Matteo";
let idade = 20;
let isProfessor = true;

console.log(nome);

document.writeln("O nome é: " + nome);

console.log(typeof (nome));
console.log(idade);
console.log(isProfessor);

console.log(2 + 2);
console.log("2" + "2");

console.log("A soma de 2 + 2 é: " + (2 + 2));

console.log(`A soma de 4 + 4 é: ${4 + 4}`);


//console.log("Alguma coisa para imprimir...");
//alert("Alguma coisa: ");

//Funções:
function escreverNoConsole() {
    console.log("Olá mundo!")
}

//chamar a função:

escreverNoConsole();

//------------------------------------
function podeDirigir(idade, cnh) {
    if (idade >= 18 && cnh == true) {
        console.log("Pode dirigir!")
    } else {
        console.log("Não pode dirigir!")
    }
}

podeDirigir(40, true);

//------------------------------------
//arrowfunction
const parOuImpar = (valor) => {
    return valor % 2;
}

console.log(parOuImpar(2));

let n = 4;

if (parOuImpar(n) == 0) {
    console.log("É par!")
    console.log(`O número ${n} é par!`);
} else {
    console.log("É impar!")
    console.log(`O número ${n} é impar!`)
}
//---------------------------------------

for (let x = 1; x <= 10; x++) {
    console.log("O valor de x é: " + x)
}

const parOuImpar2 = (valor) => {
    if (valor % 2 == 0) {
        console.log("é par!");
    } else {
        console.log("é impar!");
    }
}

//array
console.log(nome.toLocaleUpperCase());

let numeros = [1, 3, 5, 8, 12];

console.log(numeros);

//quantidade de length
console.log(numeros.length);

//pegar length específica:
console.log(numeros[0]);

let peixes = ['acara-bandeira', 'palhaço', 'esturjão', 'mandarim'];

console.log(peixes);

remover_ultimo = peixes.pop();
console.log("Peixe removido: " + remover_ultimo)

adicionar = peixes.push('Peixe Espada');
console.log("Peixe adicionado: " + adicionar);

console.log(peixes);
//------------------------------------


const aluno = { 
    nome: "Matteo",
    Sobrenome: "Nicolas"
}



let aluno = { //objeto
    nome: '',
    setNome: function(vNome){
        this.nome = vNome;
        
    },
    getNome: function(){
        return this.nome;
    }

}
class Pessoa{ //classe
    constructor(nome){
        this.nome;
    }
    get getNome(){
        return this.nome;
    }
    set setNome(value){
        this.nome = value;
    }
}